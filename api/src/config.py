import urllib

DEBUG = True # Turns on debugging features in Flask สำหรับเปิด debug
JSON_SORT_KEYS = False #การเรียง JSON
SQLALCHEMY_TRACK_MODIFICATIONS = False # ปิด Warning

# Localhost
# params = urllib.parse.quote_plus("DRIVER={SQL Server};Server=192.168.111.12;PORT=1433;Database=VGROUP_API;UID=sa;PWD=vghdb;") 
params = urllib.parse.quote_plus("DRIVER={SQL Server};Server=192.168.111.12;PORT=1433;Database=VGROUP_API;UID=sa;PWD=vghdb;") 

# Windows Server
# params = urllib.parse.quote_plus("DRIVER={SQL Server};Server=192.168.111.12;PORT=1433;Database=VGROUP_API;UID=sa;PWD=vghdb;")

# Docker Server 
# params = urllib.parse.quote_plus("DRIVER={FreeTDS};Server=192.168.111.12;PORT=1433;Database=VGROUP_API;UID=sa;PWD=vghdb;TDS_Version=8.0;")
# Docker Server local
# params = urllib.parse.quote_plus("DRIVER={FreeTDS};Server=192.168.111.12;PORT=1433;Database=VGROUP_API;UID=sa;PWD=vghdb;TDS_Version=8.0;")

# MAC
# params = urllib.parse.quote_plus("DRIVER={FreeTDS};Server=192.168.111.12;PORT=1433;Database=VGROUP_API;UID=sa;PWD=vghdb;TDS_Version=8.0;")
# 
SQLALCHEMY_DATABASE_URI = "mssql+pyodbc:///?odbc_connect=%s" % params
# SQLALCHEMY_DATABASE_URI = 'sqlite:///posts.db'