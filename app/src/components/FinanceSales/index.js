import Login from './Login'
import Branch from './Branch'
import BranchEdit from './BranchEdit'
import HeadOffice from './HeadOffice'
import Report from './Report'
import ReportCheckBooking from './ReportCheckBooking'
import UpdateCancelBooking from './UpdateCancelBooking'
import DashBoard from './DashBoard'
import AllocateOrder from './AllocateOrder'
import ManageOrder from './ManageOrder'

export default {
  Login,
  Branch,
  BranchEdit,
  HeadOffice,
  Report,
  ReportCheckBooking,
  UpdateCancelBooking,
  DashBoard,
  AllocateOrder,
  ManageOrder
}
