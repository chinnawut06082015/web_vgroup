import Chart from 'chart.js'
import { Doughnut, mixins } from 'vue-chartjs'
import ChartDataLabels from 'chartjs-plugin-datalabels'

Chart.plugins.unregister(ChartDataLabels)

const { reactiveProp } = mixins

export default {
  extends: Doughnut,
  mixins: [reactiveProp],
  props: {
    options: {
      type: Object,
      default: null
    },
    chartData: {
      type: Object,
      default: null
    }
  },

  mounted () {
    this.addPlugin(ChartDataLabels)
    this.renderChart(
      this.chartData,
      this.options
    )
  }
}
