from flask import current_app, render_template, request, redirect
from flask_limiter import Limiter
from ..models import SYS_USER_MODEL
from flask import jsonify

@current_app.route('/master_color/get', methods=['GET'])
def master_color_get():
    # ทดสอบ
    filter = request.args.to_dict()
    print('Controller', filter)
    return SYS_USER_MODEL.select_by_filter(filter)


@current_app.route('/master_color/find_advanced', methods=['GET'])
def master_color_find():

    filter = request.args.to_dict()
    print('Controller', filter)
    return SYS_USER_MODEL.select_by_all(filter)


@current_app.route('/master_color/getID', methods=['GET'])
def master_color_get_id():

    filter = request.args.to_dict()
    print('Controller', filter.get('colorID'))
    response = SYS_USER_MODEL.select_by_id(filter.get('colorID'))
    return response, 200


@current_app.route('/master_color/add', methods=['POST'])
def master_color_add():

    if request.method == 'POST':
        bodyObj = request.get_json(silent=True)
        print('bodyObj' , bodyObj)
        SYS_USER_MODEL.insert(bodyObj)
        response = {
            'status': True
        }
        return jsonify(response), 200


@current_app.route('/master_color/edit/<int:id>', methods=['POST'])
def master_color_edit(id):

    if id:

        print('ID', id)
        rs = SYS_USER_MODEL.select_by_id(id)
        print(rs)
        if rs:
            bodyObj = request.get_json(silent=True)
            SYS_USER_MODEL.update(bodyObj, id)
            response = {
                'status': True
            }
            return jsonify(response), 200


@current_app.route('/master_color/delete/<int:id>', methods=['POST'])
def master_color_delete(id):

    if id:

        print('ID', id)
        rs = SYS_USER_MODEL.select_by_id(id)
        print(rs)
        if rs:
            bodyObj = request.get_json(silent=True)
            last_user = bodyObj.get('LAST_USER')
            SYS_USER_MODEL.delete(id, last_user)
            response = {
                'status': True
            }
            return jsonify(response), 200
