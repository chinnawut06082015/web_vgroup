import Login from './Login'
import Header from './Header'
import Home from './Home'
import Sidebar from './Sidebar'
import Over150K from './Over150K'
import CampaignAdd from './CampaignAdd'
import CampaignEdit from './CampaignEdit'
import CampaignView from './CampaignView'
import FOCAdd from './FOCAdd'
import FOCEdit from './FOCEdit'
import FOCView from './FOCView'
import NetworkStaff from './NetworkStaff'
// import Search from './Search'
// import Product from './Product'
// import Category from './Category'
// import Cart from './Cart'

export default {
  Login,
  Header,
  Home,
  Sidebar,
  Over150K,
  CampaignAdd,
  CampaignEdit,
  CampaignView,
  FOCAdd,
  FOCEdit,
  FOCView,
  NetworkStaff
  // Search,
  // Product,
  // Category,
  // Cart
}
