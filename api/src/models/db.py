from flask import current_app
from flask_sqlalchemy import SQLAlchemy
from flask_marshmallow import Marshmallow 

db = SQLAlchemy(current_app)
ma = Marshmallow(current_app)