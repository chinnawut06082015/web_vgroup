import Login from './Login'
import Sidebar from './Sidebar'
import Header from './Header'
import ModelBulletin from './ModelBulletin'
import BPnonePM from './BPnonePM'
import PMBackMonth from './PMBackMonth'

export default {
  Login,
  Sidebar,
  Header,
  ModelBulletin,
  BPnonePM,
  PMBackMonth

}
