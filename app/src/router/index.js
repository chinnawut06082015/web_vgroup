import Vue from 'vue'
import Router from 'vue-router'
import Insurance from '@/components/Insurance'
import InsuranceCode4 from '@/components/InsuranceCode4'
// import Zoom from '@/components/Zoom'
import Vlink from '@/components/Vlink'
import Vsales from '@/components/Vsales'
import Vservice from '@/components/Vservice'
import VserviceBooking from '@/components/Vservice/Booking'
import VsalesMaster from '@/components/Vsales/Master'
import VsalesActivety from '@/components/Vsales/Activety'
import VsalesDashboard from '@/components/Vsales/Dashboard'
import VsalesReport from '@/components/Vsales/Report'
import ReportService from '@/components/ReportService'
import ReportParts from '@/components/ReportParts'
import ReportInsurance from '@/components/ReportInsurance'
import FinanceSales from '@/components/FinanceSales'
import Loyalty from '@/components/Loyalty'
import ReportVgroupServiceVSys from '@/components/ReportVgroup/Service/VServiceSystem'
import ReportVgroupServiceEOD from '@/components/ReportVgroup/Service/EOMDeliveryNewCars'
import ReportVgroupServiceTBA from '@/components/ReportVgroup/Service/TireBattAIR'
import ImportsData from '@/components/ImportsData'
import ReportSale from '@/components/ReportSale'
import FOCSale from '@/components/FOCSale'
// import ReportDashboard from '@/components/ReportDashboard'
import ReportDashboardBooking from '@/components/ReportDashboard/Booking'
import ReportDashboardPerformance from '@/components/ReportDashboard/Performance'
import ReportDashboardIncomingcar from '@/components/ReportDashboard/Incomingcar'
import ReportDashboardCloseJob from '@/components/ReportDashboard/CloseJob'
import ReportDashboardFRTWsBP from '@/components/ReportDashboard/FRTWsBP'
import ReportDashboardFRTWsGR from '@/components/ReportDashboard/FRTWsGR'
import ReportDashboardNewCarsCode4 from '@/components/ReportDashboard/NewCarsCode4'
import ReportDashboardUncloseJob from '@/components/ReportDashboard/UncloseJob'
import ReportDashboardTenkilo from '@/components/ReportDashboard/Tenkilo'
import ReportDashboardPerformanceCRD from '@/components/ReportDashboard/PerformanceCRD'
import ReportDashboardEfficiencyGR from '@/components/ReportDashboard/EfficiencyGR'
import ReportDashboardEfficiencyBP from '@/components/ReportDashboard/EfficiencyBP'
import ReportDashboardInsurance from '@/components/ReportDashboard/Insurance'
import FinanceSalesEdit from '@/components/FinanceSalesEdit'

Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/vlink/Questionnaire',
      name: 'Questionnaire',
      component: Vlink.Questionnaire
    },
    {
      path: '/vlink/QuestionnaireEmply',
      name: 'QuestionnaireEmply',
      component: Vlink.QuestionnaireEmply
    },
    {
      path: '/vlink/QuestionnaireEmplyPrivacy',
      name: 'QuestionnaireEmplyPrivacy',
      component: Vlink.QuestionnaireEmplyPrivacy
    },
    {
      path: '/vlink/getCoupon',
      name: 'getCoupon',
      component: Vlink.getCoupon
    },
    {
      path: '/Vservice/Login',
      name: 'Login',
      component: Vservice.Login
    },
    {
      path: '/Vservice/booking/ImportBooking',
      name: 'ImportBooking',
      component: VserviceBooking.ImportBooking
    },
    {
      path: '/vsales/Login',
      name: 'Login',
      component: Vsales.Login
    },
    {
      path: '/vsales/Home',
      name: 'Home',
      component: Vsales.Home
    },
    {
      path: '/vsales/master/MasterCar',
      name: 'MasterCar',
      component: VsalesMaster.MasterCar
    },
    {
      path: '/vsales/activety/DataFacebookPage',
      name: 'DataFacebookPage',
      component: VsalesActivety.DataFacebookPage
    },
    {
      path: '/vsales/activety/ReportFacebookPage',
      name: 'ReportFacebookPage',
      component: VsalesActivety.ReportFacebookPage
    },
    {
      path: '/vsales/dashboard/VsalesManager',
      name: 'VsalesManager',
      component: VsalesDashboard.VsalesManager
    },
    {
      path: '/vsales/dashboard/VsalesBoard',
      name: 'VsalesBoard',
      component: VsalesDashboard.VsalesBoard
    },
    {
      path: '/vsales/dashboard/VsalesFacebookPage',
      name: 'VsalesFacebookPage',
      component: VsalesDashboard.VsalesFacebookPage
    },
    {
      path: '/vsales/report/ReportBookingCheckVsale',
      name: 'ReportBookingCheckVsale',
      component: VsalesReport.ReportBookingCheckVsale
    },
    // {
    //   path: '/zoom/Login',
    //   name: 'Login',
    //   component: Zoom.Login
    // },
    // {
    //   path: '/zoom/Process',
    //   name: 'Process',
    //   component: Zoom.Process
    // },
    // {
    //   path: '/zoom/ProcessSetting',
    //   name: 'ProcessSetting',
    //   component: Zoom.ProcessSetting
    // },
    // {
    //   path: '/zoom/ProcessApprove',
    //   name: 'ProcessApprove',
    //   component: Zoom.ProcessApprove
    // },
    {
      path: '/InsuranceCode4/Login',
      name: 'Login',
      component: InsuranceCode4.Login
    },
    {
      path: '/InsuranceCode4/Home',
      name: 'Home',
      component: InsuranceCode4.Home
    },
    {
      path: '/InsuranceCode4/AllocateProspectiveCode4',
      name: 'AllocateProspectiveCode4',
      component: InsuranceCode4.AllocateProspectiveCode4
    },
    {
      path: '/InsuranceCode4/AppSalesHome',
      name: 'AppSalesHome',
      component: InsuranceCode4.AppSalesHome
    },
    {
      path: '/insurance/Login',
      name: 'Login',
      component: Insurance.Login
    },
    {
      path: '/insurance/AllocateProspectiveCode4',
      name: 'AllocateProspectiveCode4',
      component: Insurance.AllocateProspectiveCode4
    },
    {
      path: '/insurance/AllocateProspectiveNewCar',
      name: 'AllocateProspectiveNewCar',
      component: Insurance.AllocateProspectiveNewCar
    },
    {
      path: '/insurance/AllocateImportInsurance',
      name: 'AllocateImportInsurance',
      component: Insurance.AllocateImportInsurance
    },
    {
      path: '/insurance/RenotiImportInsurance',
      name: 'RenotiImportInsurance',
      component: Insurance.RenotiImportInsurance
    },
    {
      path: '/insurance/RenotiUpdateImportInsurance',
      name: 'RenotiUpdateImportInsurance',
      component: Insurance.RenotiUpdateImportInsurance
    },
    {
      path: '/insurance/AllocateProspectiveHistory',
      name: 'AllocateProspectiveHistory',
      component: Insurance.AllocateProspectiveHistory
    },
    {
      path: '/insurance/NewProcess',
      name: 'NewProcess',
      component: Insurance.NewProcess
    },
    {
      path: '/insurance/UpdateProcess',
      name: 'UpdateProcess',
      component: Insurance.UpdateProcess
    },
    {
      path: '/insurance/RenewProcess',
      name: 'RenewProcess',
      component: Insurance.RenewProcess
    },
    {
      path: '/insurance/Home',
      name: 'Home',
      component: Insurance.Home
    },
    {
      path: '/insurance/ReportEms',
      name: 'ReportEms',
      component: Insurance.ReportEms
    },
    {
      path: '/insurance/salesUpdateProspective',
      name: 'salesUpdateProspective',
      component: Insurance.salesUpdateProspective
    },
    {
      path: '/insurance/AllocateUpdateImportInsurance',
      name: 'AllocateUpdateImportInsurance',
      component: Insurance.AllocateUpdateImportInsurance
    },
    {
      path: '/insurance/ReportProcess',
      name: 'ReportProcess',
      component: Insurance.ReportProcess
    },
    {
      path: '/insurance/salesProspective',
      name: 'salesProspective',
      component: Insurance.salesProspective
    },
    {
      path: '/insurance/InsuranceNewCar',
      name: 'InsuranceNewCar',
      component: Insurance.InsuranceNewCar
    },
    {
      path: '/insurance/InsuranceWarning',
      name: 'InsuranceWarning',
      component: Insurance.InsuranceWarning
    },
    {
      path: '/insurance/InsuranceBP',
      name: 'InsuranceBP',
      component: Insurance.InsuranceBP
    },
    {
      path: '/insurance/InsuranceCreateName',
      name: 'InsuranceCreateName',
      component: Insurance.InsuranceCreateName
    },
    {
      path: '/insurance/InsuranceCode4',
      name: 'InsuranceCode4',
      component: Insurance.InsuranceCode4
    },
    {
      path: '/insurance/InsuranceFollow',
      name: 'InsuranceFollow',
      component: Insurance.InsuranceFollow
    },
    {
      path: '/insurance/ImportInsurance',
      name: 'ImportInsurance',
      component: Insurance.ImportInsurance
    },
    {
      path: '/insurance/ReportSyspro',
      name: 'ReportSyspro',
      component: Insurance.ReportSyspro
    },
    {
      path: '/insurance/ReportProspective',
      name: 'ReportProspective',
      component: Insurance.ReportProspective
    },
    {
      path: '/insurance/TestPrint',
      name: 'TestPrint',
      component: Insurance.TestPrint
    },
    {
      path: '/insurance/ReportSysproCheck',
      name: 'ReportSysproCheck',
      component: Insurance.ReportSysproCheck
    },
    {
      path: '/insurance/ReportSysproCheckProtecDate',
      name: 'ReportSysproCheckProtecDate',
      component: Insurance.ReportSysproCheckProtecDate
    },
    {
      path: '/insurance/ManageCar',
      name: 'ManageCar',
      component: Insurance.ManageCar
    },
    {
      path: '/insurance/ManageFinanceAndInsName',
      name: 'ManageFinanceAndInsName',
      component: Insurance.ManageFinanceAndInsName
    },
    {
      path: '/insurance/AccountIns',
      name: 'AccountIns',
      component: Insurance.AccountIns
    },
    {
      path: '/insurance/DashBoardDataStudio',
      name: 'DashBoardDataStudio',
      component: Insurance.DashBoardDataStudio
    },
    {
      path: '/insurance/DashBoardDataStudioDetails',
      name: 'DashBoardDataStudioDetails',
      component: Insurance.DashBoardDataStudioDetails
    },
    {
      path: '/insurance/ReportSysproCheckProspectiveCode',
      name: 'ReportSysproCheckProspectiveCode',
      component: Insurance.ReportSysproCheckProspectiveCode
    },
    {
      path: '/insurance/ManageAdressMap',
      name: 'ManageAdressMap',
      component: Insurance.ManageAdressMap
    },
    // FOCSale
    {
      path: '/FOCSale/Login',
      name: 'Login',
      component: FOCSale.Login
    },
    {
      path: '/FOCSale/Home',
      name: 'Home',
      component: FOCSale.Home
    },
    {
      path: '/FOCSale/CampaignAdd',
      name: 'CampaignAdd',
      component: FOCSale.CampaignAdd
    },
    {
      path: '/FOCSale/CampaignEdit',
      name: 'CampaignEdit',
      component: FOCSale.CampaignEdit
    },
    {
      path: '/FOCSale/CampaignView',
      name: 'CampaignView',
      component: FOCSale.CampaignView
    },
    {
      path: '/FOCSale/FOCAdd',
      name: 'FOCAdd',
      component: FOCSale.FOCAdd
    },
    {
      path: '/FOCSale/FOCEdit',
      name: 'FOCEdit',
      component: FOCSale.FOCEdit
    },
    {
      path: '/FOCSale/FOCView',
      name: 'FOCView',
      component: FOCSale.FOCView
    },
    // ReportDashboard
    {
      path: '/ReportDashboard/Booking/Login',
      name: 'Login',
      component: ReportDashboardBooking.Login
    },
    {
      path: '/ReportDashboard/Booking/Booking',
      name: 'Booking',
      component: ReportDashboardBooking.Booking
    },
    {
      path: '/ReportDashboard/Performance/Login',
      name: 'Login',
      component: ReportDashboardPerformance.Login
    },
    {
      path: '/ReportDashboard/Performance/Performance',
      name: 'Performance',
      component: ReportDashboardPerformance.Performance
    },
    {
      path: '/ReportDashboard/PerformanceCRD/Login',
      name: 'Login',
      component: ReportDashboardPerformanceCRD.Login
    },
    {
      path: '/ReportDashboard/PerformanceCRD/PerformanceCRD',
      name: 'PerformanceCRD',
      component: ReportDashboardPerformanceCRD.PerformanceCRD
    },
    {
      path: '/ReportDashboard/Incomingcar/Login',
      name: 'Login',
      component: ReportDashboardIncomingcar.Login
    },
    {
      path: '/ReportDashboard/Incomingcar/Incomingcar',
      name: 'Incomingcar',
      component: ReportDashboardIncomingcar.Incomingcar
    },
    {
      path: '/ReportDashboard/CloseJob/Login',
      name: 'Login',
      component: ReportDashboardCloseJob.Login
    },
    {
      path: '/ReportDashboard/CloseJob/CloseJob',
      name: 'CloseJob',
      component: ReportDashboardCloseJob.CloseJob
    },
    {
      path: '/ReportDashboard/FRTWsBP/Login',
      name: 'Login',
      component: ReportDashboardFRTWsBP.Login
    },
    {
      path: '/ReportDashboard/FRTWsBP/FRTWsBP',
      name: 'FRTWsBP',
      component: ReportDashboardFRTWsBP.FRTWsBP
    },
    {
      path: '/ReportDashboard/FRTWsGR/Login',
      name: 'Login',
      component: ReportDashboardFRTWsGR.Login
    },
    {
      path: '/ReportDashboard/FRTWsGR/FRTWsGR',
      name: 'FRTWsGR',
      component: ReportDashboardFRTWsGR.FRTWsGR
    },
    {
      path: '/ReportDashboard/NewCarsCode4/Login',
      name: 'Login',
      component: ReportDashboardNewCarsCode4.Login
    },
    {
      path: '/ReportDashboard/NewCarsCode4/NewCarsCode4',
      name: 'NewCarsCode4',
      component: ReportDashboardNewCarsCode4.NewCarsCode4
    },
    {
      path: '/ReportDashboard/UncloseJob/Login',
      name: 'Login',
      component: ReportDashboardUncloseJob.Login
    },
    {
      path: '/ReportDashboard/UncloseJob/UncloseJob',
      name: 'UncloseJob',
      component: ReportDashboardUncloseJob.UncloseJob
    },
    {
      path: '/ReportDashboard/Tenkilo/Login',
      name: 'Login',
      component: ReportDashboardTenkilo.Login
    },
    {
      path: '/ReportDashboard/Tenkilo/Tenkilo',
      name: 'Tenkilo',
      component: ReportDashboardTenkilo.Tenkilo
    },
    {
      path: '/ReportDashboard/EfficiencyGR/Login',
      name: 'Login',
      component: ReportDashboardEfficiencyGR.Login
    },
    {
      path: '/ReportDashboard/EfficiencyGR/EfficiencyGR',
      name: 'EfficiencyGR',
      component: ReportDashboardEfficiencyGR.EfficiencyGR
    },
    {
      path: '/ReportDashboard/EfficiencyBP/Login',
      name: 'Login',
      component: ReportDashboardEfficiencyBP.Login
    },
    {
      path: '/ReportDashboard/EfficiencyBP/EfficiencyBP',
      name: 'EfficiencyBP',
      component: ReportDashboardEfficiencyBP.EfficiencyBP
    },
    {
      path: '/ReportDashboard/Insurance/Login',
      name: 'Login',
      component: ReportDashboardInsurance.Login
    },
    {
      path: '/ReportDashboard/Insurance/Insurance',
      name: 'Insurance',
      component: ReportDashboardInsurance.Insurance
    },
    // reportService
    {
      path: '/reportService/Login',
      name: 'Login',
      component: ReportService.Login
    },
    {
      path: '/reportService/Home',
      name: 'Home',
      component: ReportService.Home
    },
    {
      path: '/reportService/ModelBulletin',
      name: 'ModelBulletin',
      component: ReportService.ModelBulletin
    },
    {
      path: '/reportService/ModelBulletinNoex',
      name: 'ModelBulletinNoex',
      component: ReportService.ModelBulletinNoex
    },
    {
      path: '/reportService/BPnonePM',
      name: 'BPnonePM',
      component: ReportService.BPnonePM
    },
    {
      path: '/reportService/BPnonePMNoex',
      name: 'BPnonePMNoex',
      component: ReportService.BPnonePMNoex
    },
    {
      path: '/reportService/PMBackMonth',
      name: 'PMBackMonth',
      component: ReportService.PMBackMonth
    },
    {
      path: '/reportService/PMBackMonthNoex',
      name: 'PMBackMonthNoex',
      component: ReportService.PMBackMonthNoex
    },
    {
      path: '/reportService/InsnoneBP',
      name: 'InsnoneBP',
      component: ReportService.InsnoneBP
    },
    {
      path: '/reportService/InsnoneBPNoex',
      name: 'InsnoneBPNoex',
      component: ReportService.InsnoneBPNoex
    },
    {
      path: '/reportService/EOMNewCars',
      name: 'EOMNewCars',
      component: ReportService.EOMNewCars
    },
    {
      path: '/reportService/FavoriteClaim',
      name: 'FavoriteClaim',
      component: ReportService.FavoriteClaim
    },
    {
      path: '/reportService/EOMNewCarsNoex',
      name: 'EOMNewCarsNoex',
      component: ReportService.EOMNewCarsNoex
    },
    {
      path: '/reportService/DetailCM',
      name: 'DetailCM',
      component: ReportService.DetailCM
    },
    {
      path: '/reportService/DetailDZone',
      name: 'DetailDZone',
      component: ReportService.DetailDZone
    },
    // ReportParts
    {
      path: '/ReportParts/Login',
      name: 'Login',
      component: ReportParts.Login
    },
    {
      path: '/ReportParts/Home',
      name: 'Home',
      component: ReportParts.Home
    },
    {
      path: '/ReportParts/SubKPIHonda',
      name: 'SubKPIHonda',
      component: ReportParts.SubKPIHonda
    },
    // ReportSale
    {
      path: '/ReportSale/Login',
      name: 'Login',
      component: ReportSale.Login
    },
    {
      path: '/ReportSale/Home',
      name: 'Home',
      component: ReportSale.Home
    },
    {
      path: '/ReportSale/Over150K',
      name: 'Over150K',
      component: ReportSale.Over150K
    },
    {
      path: '/ReportSale/Network',
      name: 'Network',
      component: ReportSale.Network
    },
    {
      path: '/ReportSale/NetworkStaff',
      name: 'NetworkStaff',
      component: ReportSale.NetworkStaff
    },
    // ReportVgroup/Service
    {
      path: '/ReportVgroup/Service/EOMDeliveryNewCars/Login',
      name: 'Login',
      component: ReportVgroupServiceEOD.Login
    },
    {
      path: '/ReportVgroup/Service/EOMDeliveryNewCars/EOMNewCars',
      name: 'EOMNewCars',
      component: ReportVgroupServiceEOD.EOMNewCars
    },
    {
      path: '/ReportVgroup/Service/TireBattAIR/Login',
      name: 'Login',
      component: ReportVgroupServiceTBA.Login
    },
    {
      path: '/ReportVgroup/Service/TireBattAIR/TireBattAIR',
      name: 'EOMNewCars',
      component: ReportVgroupServiceTBA.TireBattAIR
    },
    {
      path: '/ReportVgroup/Service/VServiceSystem/Login',
      name: 'Login',
      component: ReportVgroupServiceVSys.Login
    },
    {
      path: '/ReportVgroup/Service/VServiceSystem/ModelBulletin',
      name: 'ModelBulletin',
      component: ReportVgroupServiceVSys.ModelBulletin
    },
    {
      path: '/ReportVgroup/Service/VServiceSystem/BPnonePM',
      name: 'BPnonePM',
      component: ReportVgroupServiceVSys.BPnonePM
    },
    {
      path: '/ReportVgroup/Service/VServiceSystem/PMBackMonth',
      name: 'PMBackMonth',
      component: ReportVgroupServiceVSys.PMBackMonth
    },
    // ReportInsurance
    {
      path: '/ReportInsurance/Login',
      name: 'Login',
      component: ReportInsurance.Login
    },
    {
      path: '/ReportInsurance/CarsCode4',
      name: 'CarsCode4',
      component: ReportInsurance.CarsCode4
    },
    {
      path: '/ReportInsurance/ModelBulletinIns',
      name: 'ModelBulletinIns',
      component: ReportInsurance.ModelBulletinIns
    },
    {
      path: '/financeSales/Login',
      name: 'Login',
      component: FinanceSales.Login
    },
    {
      path: '/financeSales/DashBoard',
      name: 'DashBoard',
      component: FinanceSales.DashBoard
    },
    {
      path: '/financeSales/Branch',
      name: 'Branch',
      component: FinanceSales.Branch
    },
    {
      path: '/financeSales/BranchEdit',
      name: 'BranchEdit',
      component: FinanceSales.BranchEdit
    },
    {
      path: '/financeSales/HeadOffice',
      name: 'HeadOffice',
      component: FinanceSales.HeadOffice
    },
    {
      path: '/financeSales/Report',
      name: 'Report',
      component: FinanceSales.Report
    },
    {
      path: '/financeSales/AllocateOrder',
      name: 'AllocateOrder',
      component: FinanceSales.AllocateOrder
    },
    {
      path: '/financeSales/ReportCheckBooking',
      name: 'ReportCheckBooking',
      component: FinanceSales.ReportCheckBooking
    },
    {
      path: '/financeSales/UpdateCancelBooking',
      name: 'UpdateCancelBooking',
      component: FinanceSales.UpdateCancelBooking
    },
    {
      path: '/financeSales/ManageOrder',
      name: 'ManageOrder',
      component: FinanceSales.ManageOrder
    },
    {
      path: '/financeSalesEdit/Login',
      name: 'Login',
      component: FinanceSalesEdit.Login
    },
    {
      path: '/financeSalesEdit/SaleEdit',
      name: 'SaleEdit',
      component: FinanceSalesEdit.SaleEdit
    },
    {
      path: '/vsales/dashboard/Incomingcar',
      name: 'Incomingcar',
      component: VsalesDashboard.Incomingcar
    },
    {
      path: '/vsales/dashboard/CloseJob',
      name: 'CloseJob',
      component: VsalesDashboard.CloseJob
    },
    {
      path: '/vsales/dashboard/Tenkilo',
      name: 'Tenkilo',
      component: VsalesDashboard.Tenkilo
    },
    {
      path: '/vsales/dashboard/NewCarsCode4',
      name: 'NewCarsCode4',
      component: VsalesDashboard.NewCarsCode4
    },
    {
      path: '/vsales/dashboard/Booking',
      name: 'Booking',
      component: VsalesDashboard.Booking
    },
    {
      path: '/vsales/dashboard/UncloseJob',
      name: 'UncloseJob',
      component: VsalesDashboard.UncloseJob
    },
    {
      path: '/vsales/dashboard/Performance',
      name: 'Performance',
      component: VsalesDashboard.Performance
    },
    {
      path: '/vsales/dashboard/FRTWsGR',
      name: 'FRTWsGR',
      component: VsalesDashboard.FRTWsGR
    },
    {
      path: '/vsales/dashboard/FRTWsBP',
      name: 'FRTWsBP',
      component: VsalesDashboard.FRTWsBP
    },
    {
      path: '/vsales/dashboard/Hrm',
      name: 'Hrm',
      component: VsalesDashboard.Hrm
    },
    {
      path: '/vsales/dashboard/InsuranceOverview',
      name: 'InsuranceOverview',
      component: VsalesDashboard.InsuranceOverview
    },
    {
      path: '/vsales/dashboard/HouseD',
      name: 'HouseD',
      component: VsalesDashboard.HouseD
    },
    {
      path: '/Loyalty/ReportSyspro',
      name: 'ReportSyspro',
      component: Loyalty.ReportSyspro
    },
    // ImportsData/
    {
      path: '/ImportsData/Login',
      name: 'Login',
      component: ImportsData.Login
    },
    {
      path: '/ImportsData/ImportAgeing',
      name: 'ImportAgeing',
      component: ImportsData.ImportAgeing
    },
    {
      path: '/ImportsData/ImportAccident',
      name: 'ImportAccident',
      component: ImportsData.ImportAccident
    },
    {
      path: '/vsales/dashboard/Gafana',
      name: 'Gafana',
      component: VsalesDashboard.Gafana
    },
    {
      path: '/insurance/ReportCom',
      name: 'ReportCom',
      component: Insurance.ReportCom
    }
  ]
})
