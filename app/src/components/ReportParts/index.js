import Login from './Login'
import Home from './Home'
import Sidebar from './Sidebar'
import SubKPIHonda from './SubKPIHonda'

export default {
  Login,
  Home,
  Sidebar,
  SubKPIHonda
}
