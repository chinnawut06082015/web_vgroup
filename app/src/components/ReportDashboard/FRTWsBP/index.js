import Login from './Login'
import Sidebar from './Sidebar'
import FRTWsBP from './FRTWsBP'

export default {
  Login,
  Sidebar,
  FRTWsBP

}
