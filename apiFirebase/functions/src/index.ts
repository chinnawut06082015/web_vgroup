import * as functions from 'firebase-functions';
const REGION: any = process.env.FUNCTION_REGION || "asia-east2"
// export const onCall = functions.region(REGION).https.onCall
// export const onRequest = functions.region(REGION).runWith({timeoutSeconds: 60, memory: '512MB'}).https.onRequest
const express = require('express');
const cors = require('cors');
const app = express();
const request = require('request')

app.use(cors({ origin: true }));
// build multiple CRUD interfaces:
app.get('/', (req: any, res: any) => {
  res.send('Hello GET by noomerZx')
});
app.post('/noti', (req: any, res: any) => {
    request({
        method: 'POST',
        uri: 'https://notify-api.line.me/api/notify',
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded'
        },
        auth: {
          'bearer': req.body['token']
        },
        form: {
          message: req.body['message']
        }
      }, (err: any, httpResponse: any, body: any) => {
        if(err){
          console.log(err);
        } else {
          res.json({
            httpResponse: httpResponse,
            body: body
          });
        }
      });
//   res.send('Hello POST by noomerZx')
});
// app.put('/', (req, res) => {
//   res.send('Hello PUT by noomerZx')
// });
// app.patch('/', (req, res) => {
//   res.send('Hello PATCH by noomerZx')
// });
// app.delete('/', (req, res) => {
//   res.send('Hello DELETE by noomerZx')
// });
// Expose Express API as a single Cloud Function:
exports.api = functions.region(REGION).runWith({timeoutSeconds: 60, memory: '1GB'}).https.onRequest(app);
exports.Helpdesk = require('./Helpdesk')