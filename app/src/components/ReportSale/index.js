import Login from './Login'
import Header from './Header'
import Home from './Home'
import Sidebar from './Sidebar'
import Over150K from './Over150K'
import Network from './Network'
import NetworkStaff from './NetworkStaff'

export default {
  Login,
  Header,
  Home,
  Sidebar,
  Over150K,
  Network,
  NetworkStaff
}
