import Login from './Login'
import Sidebar from './Sidebar'
import CloseJob from './CloseJob'

export default {
  Login,
  Sidebar,
  CloseJob

}
