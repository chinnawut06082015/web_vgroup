from flask import current_app, render_template, request, redirect
from flask_limiter import Limiter
from ..models import SYS_USER_MODEL
from flask import jsonify

@current_app.route('/system_user/get', methods=['GET'])
def system_user_get():
    # ทดสอบ
    filter = request.args.to_dict()
    print('Controller', filter)
    return SYS_USER_MODEL.select_by_filter(filter)


@current_app.route('/system_user/find_advanced', methods=['GET'])
def system_user_find():

    filter = request.args.to_dict()
    print('Controller', filter)
    return SYS_USER_MODEL.select_by_all(filter)


@current_app.route('/system_user/getID', methods=['GET'])
def system_user_get_id():

    filter = request.args.to_dict()
    print('Controller', filter.get('occupationID'))
    response = SYS_USER_MODEL.select_by_id(filter.get('occupationID'))
    return response, 200


@current_app.route('/system_user/add', methods=['POST'])
def system_user_add():

    if request.method == 'POST':
        bodyObj = request.get_json(silent=True)
        print('bodyObj' , bodyObj)
        SYS_USER_MODEL.insert(bodyObj)
        response = {
            'status': True
        }
        return jsonify(response), 200


@current_app.route('/system_user/edit/<int:id>', methods=['POST'])
def system_user_edit(id):

    if id:

        print('ID', id)
        rs = SYS_USER_MODEL.select_by_id(id)
        print(rs)
        if rs:
            bodyObj = request.get_json(silent=True)
            SYS_USER_MODEL.update(bodyObj, id)
            response = {
                'status': True
            }
            return jsonify(response), 200


@current_app.route('/system_user/delete/<int:id>', methods=['POST'])
def system_user_delete(id):

    if id:

        print('ID', id)
        rs = SYS_USER_MODEL.select_by_id(id)
        print(rs)
        if rs:
            bodyObj = request.get_json(silent=True)
            last_user = bodyObj.get('LAST_USER')
            SYS_USER_MODEL.delete(id, last_user)
            response = {
                'status': True
            }
            return jsonify(response), 200
