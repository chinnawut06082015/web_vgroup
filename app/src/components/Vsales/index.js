import Login from './Login'
import Home from './Home'
import ManageUser from './ManageUser'

export default {
  Login,
  Home,
  ManageUser
}
