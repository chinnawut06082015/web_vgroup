import Login from './Login'
import Sidebar from './Sidebar'
import Booking from './Booking'

export default {
  Login,
  Sidebar,
  Booking

}
