import Login from './Login'
import Process from './Process'
import ProcessApprove from './ProcessApprove'
import ProcessSetting from './ProcessSetting'

export default {
  Login,
  Process,
  ProcessApprove,
  ProcessSetting
}
