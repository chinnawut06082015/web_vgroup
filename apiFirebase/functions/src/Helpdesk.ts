import * as functions from 'firebase-functions'
// import * as admin from 'firebase-admin'
// import db from './store';
// import * as cors from 'cors'

const REGION: any = process.env.FUNCTION_REGION || "asia-east2"
export const onCall = functions.region(REGION).https.onCall
export const onRequest = functions.region(REGION).runWith({ timeoutSeconds: 300, memory: '1GB' }).https.onRequest
// var database = db.collection("Prospective")

import { BigQuery } from '@google-cloud/bigquery'
const bigquery: BigQuery = new BigQuery({ projectId: 'vgroup-honda' })

exports.bq_selete_branch_FollowInvoice = onRequest(async (req, res) => {
        // var limitNo: number = Number(req.query.limitNo)
        const sql = "SELECT * FROM `vgroup-honda.Helpdesk.Process`"
        // const sql = "SELECT * FROM `betask-crm.firestore_export.Prospective` where userName='" + req.query.userName + "'  order by createDate desc LIMIT " + limitNo + ""

        const options = {
                query: sql,
                timeoutMs: 100000, // Time out after 100 seconds.
                useLegacySql: false, // Use standard SQL syntax for queries.
                location: 'US',
        };
        // Run the query
        const [rows] = await bigquery.query(options);

        console.log('Rows:');
        rows.forEach(row => console.log(`${row.subject}: ${row.num_duplicates}`));
        // Print the results
        console.log('Rows:');
        rows.forEach(row => console.log(row));
        res.set("Access-Control-Allow-Origin", "*")
        res.status(200).send(rows).end()

})