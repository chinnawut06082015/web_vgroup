import VsalesManager from './VsalesManager'
import VsalesBoard from './VsalesBoard'
import CloseJob from './CloseJob'
import Incomingcar from './Incomingcar'
import Performance from './Performance'
import Tenkilo from './Tenkilo'
import Booking from './Booking'
import UncloseJob from './UncloseJob'
import NewCarsCode4 from './NewCarsCode4'
import Gafana from './Gafana'
import FRTWsGR from './FRTWsGR'
import FRTWsBP from './FRTWsBP'
import Hrm from './Hrm'
import HouseD from './HouseD'
import InsuranceOverview from './InsuranceOverview'
import VsalesFacebookPage from './VsalesFacebookPage'

export default {
  VsalesManager,
  VsalesBoard,
  CloseJob,
  Incomingcar,
  Performance,
  Tenkilo,
  Booking,
  UncloseJob,
  NewCarsCode4,
  Gafana,
  FRTWsGR,
  FRTWsBP,
  Hrm,
  HouseD,
  InsuranceOverview,
  VsalesFacebookPage
}
