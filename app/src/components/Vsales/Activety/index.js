import DataFacebookPage from './DataFacebookPage'
import ReportFacebookPage from './ReportFacebookPage'

export default {
  DataFacebookPage,
  ReportFacebookPage
}
