import Login from './Login'
import Sidebar from './Sidebar'
import EfficiencyGR from './EfficiencyGR'

export default {
  Login,
  Sidebar,
  EfficiencyGR

}
