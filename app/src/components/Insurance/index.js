import Login from './Login'
import InsuranceNewCar from './InsuranceNewCar'
import InsuranceWarning from './InsuranceWarning'
import InsuranceBP from './InsuranceBP'
import InsuranceCreateName from './InsuranceCreateName'
import InsuranceCode4 from './InsuranceCode4'
import InsuranceFollow from './InsuranceFollow'
import ImportInsurance from './ImportInsurance'
import ReportSyspro from './ReportSyspro'
import AllocateProspectiveCode4 from './AllocateProspectiveCode4'
import AllocateProspectiveNewCar from './AllocateProspectiveNewCar'
import salesProspective from './salesProspective'
import AllocateImportInsurance from './AllocateImportInsurance'
import AllocateProspectiveHistory from './AllocateProspectiveHistory'
import NewProcess from './NewProcess'
import UpdateProcess from './UpdateProcess'
import RenewProcess from './RenewProcess'
import Home from './Home'
import salesUpdateProspective from './salesUpdateProspective'
import AllocateUpdateImportInsurance from './AllocateUpdateImportInsurance'
import ReportProcess from './ReportProcess'
import ReportProspective from './ReportProspective'
import TestPrint from './TestPrint'
import ReportSysproCheck from './ReportSysproCheck'
import ReportEms from './ReportEms'
import ReportCom from './ReportCom'
import ManageCar from './ManageCar'
import AccountIns from './AccountIns'
import DashBoardDataStudio from './DashBoardDataStudio'
import DashBoardDataStudioDetails from './DashBoardDataStudioDetails'
import ManageFinanceAndInsName from './ManageFinanceAndInsName'
import RenotiImportInsurance from './RenotiImportInsurance'
import RenotiUpdateImportInsurance from './RenotiUpdateImportInsurance'
import ReportSysproCheckProtecDate from './ReportSysproCheckProtecDate'
import ReportSysproCheckProspectiveCode from './ReportSysproCheckProspectiveCode'
import ManageAdressMap from './ManageAdressMap'

export default {
  Login,
  InsuranceNewCar,
  InsuranceWarning,
  InsuranceBP,
  InsuranceCreateName,
  InsuranceCode4,
  InsuranceFollow,
  ReportSyspro,
  ImportInsurance,
  AllocateProspectiveCode4,
  AllocateProspectiveNewCar,
  salesProspective,
  AllocateImportInsurance,
  AllocateProspectiveHistory,
  NewProcess,
  UpdateProcess,
  RenewProcess,
  Home,
  salesUpdateProspective,
  AllocateUpdateImportInsurance,
  ReportProcess,
  ReportProspective,
  TestPrint,
  ReportSysproCheck,
  ReportEms,
  ReportCom,
  ManageCar,
  AccountIns,
  DashBoardDataStudio,
  DashBoardDataStudioDetails,
  ManageFinanceAndInsName,
  RenotiImportInsurance,
  RenotiUpdateImportInsurance,
  ReportSysproCheckProtecDate,
  ReportSysproCheckProspectiveCode,
  ManageAdressMap
}
