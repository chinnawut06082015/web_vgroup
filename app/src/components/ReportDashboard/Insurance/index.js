import Login from './Login'
import Sidebar from './Sidebar'
import Insurance from './Insurance'

export default {
  Login,
  Sidebar,
  Insurance

}
