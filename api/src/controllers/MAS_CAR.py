from flask import current_app, render_template, request, redirect
from flask_limiter import Limiter
from ..models import MAS_CAR_MODEL
from flask import jsonify

@current_app.route('/MAS_CAR_MODEL/get', methods=['GET'])
def MAS_CAR_MODEL_get():
    # ทดสอบ
    filter = request.args.to_dict()
    print('Controller', filter)
    return MAS_CAR_MODEL.select_by_filter(filter)


@current_app.route('/MAS_CAR_MODEL/find_advanced', methods=['GET'])
def MAS_CAR_MODEL_find():

    filter = request.args.to_dict()
    print('Controller', filter)
    return MAS_CAR_MODEL.select_by_all(filter)


@current_app.route('/MAS_CAR_MODEL/getID', methods=['GET'])
def MAS_CAR_MODEL_get_id():

    filter = request.args.to_dict()
    print('Controller', filter.get('colorID'))
    response = MAS_CAR_MODEL.select_by_id(filter.get('colorID'))
    return response, 200


@current_app.route('/MAS_CAR_MODEL/add', methods=['POST'])
def MAS_CAR_MODEL_add():

    if request.method == 'POST':
        bodyObj = request.get_json(silent=True)
        print('bodyObj' , bodyObj)
        MAS_CAR_MODEL.insert(bodyObj)
        response = {
            'status': True
        }
        return jsonify(response), 200


@current_app.route('/MAS_CAR_MODEL/edit/<int:id>', methods=['POST'])
def MAS_CAR_MODEL_edit(id):

    if id:

        print('ID', id)
        rs = MAS_CAR_MODEL.select_by_id(id)
        print(rs)
        if rs:
            bodyObj = request.get_json(silent=True)
            MAS_CAR_MODEL.update(bodyObj, id)
            response = {
                'status': True
            }
            return jsonify(response), 200


@current_app.route('/MAS_CAR_MODEL/delete/<int:id>', methods=['POST'])
def MAS_CAR_MODEL_delete(id):

    if id:

        print('ID', id)
        rs = MAS_CAR_MODEL.select_by_id(id)
        print(rs)
        if rs:
            bodyObj = request.get_json(silent=True)
            last_user = bodyObj.get('LAST_USER')
            MAS_CAR_MODEL.delete(id, last_user)
            response = {
                'status': True
            }
            return jsonify(response), 200
