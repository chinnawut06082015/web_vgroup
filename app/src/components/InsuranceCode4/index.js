import Login from './Login'
import AllocateProspectiveCode4 from './AllocateProspectiveCode4'
import AppSalesHome from './AppSalesHome'
// import salesProspective from './salesProspective'
// import NewProcess from './NewProcess'
// import UpdateProcess from './UpdateProcess'
// import RenewProcess from './RenewProcess'
import Home from './Home'
// import salesUpdateProspective from './salesUpdateProspective'

export default {
  Login,
  AllocateProspectiveCode4,
  AppSalesHome,
  // NewProcess,
  // UpdateProcess,
  Home
}
