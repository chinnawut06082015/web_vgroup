from flask import current_app, render_template, request, redirect
from flask_limiter import Limiter
from ..models import MAS_OCCUPATION_MODEL
from flask import jsonify


@current_app.route('/master_occupation/get', methods=['GET'])
def master_occupation_get():
    # ทดสอบ
    filter = request.args.to_dict()
    print('Controller', filter)
    return MAS_OCCUPATION_MODEL.select_by_filter(filter)


@current_app.route('/master_occupation/find_advanced', methods=['GET'])
def master_occupation_find():

    filter = request.args.to_dict()
    print('Controller', filter)
    return MAS_OCCUPATION_MODEL.select_by_all(filter)


@current_app.route('/master_occupation/getID', methods=['GET'])
def master_occupation_get_id():

    filter = request.args.to_dict()
    print('Controller', filter.get('occupationID'))
    response = MAS_OCCUPATION_MODEL.select_by_id(filter.get('occupationID'))
    return response, 200


@current_app.route('/master_occupation/add', methods=['POST'])
def master_occupation_add():

    if request.method == 'POST':
        bodyObj = request.get_json(silent=True)
        print('bodyObj', bodyObj)
        MAS_OCCUPATION_MODEL.insert(bodyObj)
        response = {
            'status': True
        }
        return jsonify(response), 200


@current_app.route('/master_occupation/edit/<int:id>', methods=['POST'])
def master_occupation_edit(id):

    if id:

        print('ID', id)
        rs = MAS_OCCUPATION_MODEL.select_by_id(id)
        print(rs)
        if rs:
            bodyObj = request.get_json(silent=True)
            MAS_OCCUPATION_MODEL.update(bodyObj, id)
            response = {
                'status': True
            }
            return jsonify(response), 200


@current_app.route('/master_occupation/delete/<int:id>', methods=['POST'])
def master_occupation_delete(id):

    if id:

        print('ID', id)
        rs = MAS_OCCUPATION_MODEL.select_by_id(id)
        print(rs)
        if rs:
            bodyObj = request.get_json(silent=True)
            last_user = bodyObj.get('LAST_USER')
            MAS_OCCUPATION_MODEL.delete(id, last_user)
            response = {
                'status': True
            }
            return jsonify(response), 200
