from flask import current_app
from flask_sqlalchemy import SQLAlchemy
from datetime import datetime
from flask import jsonify
import json
from .convertData import dictDecimalToInt
db = SQLAlchemy(current_app)

db_contruct = 'MAS_OCCUPATION'
db_pk = 'occupationID'
date_now = datetime.now().strftime('%Y-%m-%d %H:%M:%S')


def select_by_all(filter=None):
    sqla = ''
    sqlb = ''
    sqlc = ''
    sql = ''
    sqla = "SELECT * "
    sqla += "FROM VW_{} ". format(db_contruct)
    sqla += "WHERE RECORD_STATUS IN ('N') "
    sqla += "AND ("
    if filter:
        for k, v in filter.items():
            sqlb += " ({0} LIKE '%%{1}%%') OR ".format(k, v)

    sqlc += "AND (RECORD_STATUS IN ('N')) "
    sqlc += " ) "
    sqlc += "ORDER BY {} DESC".format(db_pk, id)
    sql = "{} {} {}".format(sqla,sqlb[:-3],sqlc)
    print(sql)
    result = db.engine.execute(sql)
    result_data = [dictDecimalToInt(dict(r)) for r in result]
    if(len(result_data) > 0):
        return jsonify(result_data)
    else:
        return None


def select_by_filter(filter=None):
    sql = " SELECT * "
    sql += " FROM VW_{} ". format(db_contruct)
    sql += "WHERE RECORD_STATUS IN ('N')"
    print(filter)
    if filter:
        for k, v in filter.items():
            sql += " AND {0} LIKE '%%{1}%%' ".format(k, v)
    sql += "ORDER BY {} DESC".format(db_pk, id)
    print(sql)
    result = db.engine.execute(sql)
    result_data = [dictDecimalToInt(dict(r)) for r in result]
    if(len(result_data) > 0):
        return jsonify(result_data)
    else:
        return None


def select_by_id(id=None):
    sql = " SELECT * "
    sql += " FROM VW_{} ". format(db_contruct)
    sql += "WHERE RECORD_STATUS IN ('N')"
    sql += "AND {} IN ('{}') ".format(db_pk, id)
    print(sql)
    result = db.engine.execute(sql)
    result_data = [dictDecimalToInt(dict(r)) for r in result]
    if(len(result_data) > 0):
        return jsonify(result_data[0])
    else:
        return None

def insert(filter=None):
    print('insert filter : ', filter)
    sql_fields = ''
    sql_value = ''
    if filter:
        for k, v in filter.items():
                sql_fields += " {}, ".format(k)
                sql_value += " '{}', ".format(v)
    sql_fields += " RECORD_STATUS, CREATE_DATE, LAST_DATE "
    sql_value += " 'N', '{}', '{}' ".format(date_now, date_now)
    sql = "INSERT INTO {} ({}) VALUES ({})". format(db_contruct,sql_fields,sql_value)
    print('SQL : ', sql)
    return db.engine.execute(sql)

def update(filter=None, id=None):
    print('update id : ', id)
    print('update filter : ', filter)
    sql = " UPDATE {} SET ". format(db_contruct)
    if filter:
        for k, v in filter.items():
            if v != '':
                sql += " {0} = '{1}', ".format(k, v)
    sql += "LAST_DATE = '{}'".format(date_now)
    sql += "WHERE RECORD_STATUS IN ('N')"
    sql += "AND {} IN ('{}')".format(db_pk, id)
    print('SQL : ', sql)
    return db.engine.execute(sql)


def delete(id=None, user=None):
    print('Delete id : ', id)
    sql = " UPDATE {} SET ". format(db_contruct)
    sql += "RECORD_STATUS = 'D' , "
    sql += "LAST_USER = '{}' , ".format(user)
    sql += "LAST_DATE = '{}' ".format(date_now)
    sql += "WHERE RECORD_STATUS IN ('N')"
    sql += "AND {} IN ('{}')".format(db_pk, id)
    print('SQL : ', sql)
    return db.engine.execute(sql)