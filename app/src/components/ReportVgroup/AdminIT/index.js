import Login from './Login'
import Header from './Header'
import Home from './Home'
import Sidebar from './Sidebar'
import LogSignIn from './LogSignIn'

export default {
  Login,
  Header,
  Home,
  Sidebar,
  LogSignIn
}
