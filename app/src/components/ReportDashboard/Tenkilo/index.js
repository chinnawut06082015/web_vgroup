import Login from './Login'
import Sidebar from './Sidebar'
import Tenkilo from './Tenkilo'

export default {
  Login,
  Sidebar,
  Tenkilo

}
