import Login from './Login'
import Sidebar from './Sidebar'
import EfficiencyBP from './EfficiencyBP'

export default {
  Login,
  Sidebar,
  EfficiencyBP

}
