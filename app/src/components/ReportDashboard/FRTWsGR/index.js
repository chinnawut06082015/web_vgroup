import Login from './Login'
import Sidebar from './Sidebar'
import FRTWsGR from './FRTWsGR'

export default {
  Login,
  Sidebar,
  FRTWsGR

}
