import Login from './Login'
import Sidebar from './Sidebar'
import Performance from './Performance'

export default {
  Login,
  Sidebar,
  Performance

}
