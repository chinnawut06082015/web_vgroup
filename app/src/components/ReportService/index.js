import Login from './Login'
import Header from './Header'
import Home from './Home'
import Sidebar from './Sidebar'
import ModelBulletin from './ModelBulletin'
import BPnonePM from './BPnonePM'
import PMBackMonth from './PMBackMonth'
import InsnoneBP from './InsnoneBP'
import EOMNewCars from './EOMNewCars'
import ModelBulletinNoex from './ModelBulletinNoex'
import BPnonePMNoex from './BPnonePMNoex'
import PMBackMonthNoex from './PMBackMonthNoex'
import InsnoneBPNoex from './InsnoneBPNoex'
import EOMNewCarsNoex from './EOMNewCarsNoex'
import FavoriteClaim from './FavoriteClaim'
import DetailCM from './DetailCM'
import DetailDZone from './DetailDZone'

export default {
  Login,
  Header,
  Home,
  Sidebar,
  ModelBulletin,
  BPnonePM,
  PMBackMonth,
  InsnoneBP,
  EOMNewCars,
  ModelBulletinNoex,
  BPnonePMNoex,
  PMBackMonthNoex,
  InsnoneBPNoex,
  EOMNewCarsNoex,
  FavoriteClaim,
  DetailCM,
  DetailDZone
}
