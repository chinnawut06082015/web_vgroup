import Login from './Login'
import Header from './Header'
import Sidebar from './Sidebar'
import CarsCode4 from './CarsCode4'
import ModelBulletinIns from './ModelBulletinIns'

export default {
  Login,
  Header,
  Sidebar,
  CarsCode4,
  ModelBulletinIns

}
