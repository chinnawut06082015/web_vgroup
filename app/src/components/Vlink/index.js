import Questionnaire from './Questionnaire'
import QuestionnaireEmply from './QuestionnaireEmply'
import QuestionnaireEmplyPrivacy from './QuestionnaireEmplyPrivacy'
import getCoupon from './getCoupon'

export default {
  Questionnaire,
  QuestionnaireEmply,
  QuestionnaireEmplyPrivacy,
  getCoupon
}
