import Login from './Login'
import Header from './Header'
import Sidebar from './Sidebar'
import ImportOBS from './ImportOBS'
import Import010 from './Import010'
import ImportAccident from './ImportAccident'
import ImportAgeing from './ImportAgeing'

export default {
  Login,
  Header,
  Sidebar,
  ImportOBS,
  Import010,
  ImportAgeing,
  ImportAccident
}
