// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import vuetify from '@/plugins/vuetify' // path to vuetify export
import VueSweetAlert from 'vue-sweetalert'
import VueSession from 'vue-session'
import VueMask from 'v-mask'
import 'bootstrap/dist/css/bootstrap.css'
import vueXlsxTable from 'vue-xlsx-table'
import moment from 'moment' // แปลง date

Vue.config.productionTip = false
Vue.use(VueSweetAlert)
Vue.use(VueSession)
Vue.use(VueMask)
Vue.use(vueXlsxTable, {rABS: false})

// Vue.prototype.$apiUrl = 'http://localhost/vgroupl_api/v1/'
Vue.prototype.$apiUrl = 'https://api.betaskthai.com/vgroupl_api/v1/'
Vue.prototype.$apiToken = 'YWRiNjBhZTY4NDY4YjYyY2ZhZGU3N2NkMTkxOTMwYTg1YmQ5NDhhOGQ4NDE0N2ZiYzZhNTNkZmIzMzUzZTc4Yw'
Vue.prototype.$liff = window.liff
Vue.prototype.$liff_question = '1653760842-ykzLVkrj'
Vue.config.productionTip = false
Vue.mixin({
  data: function () {
    return {
      globalVar: 'global',
      IPPotocalENV_Production: 'https://api.betaskthai.com/vgroupl_api/',
      IPPotocalENV_Developer: 'http://localhost/vgroupl_api/',
      DNS_IP: '',
      // ประเภทลูกค้า
      optionsSearch: [
        { text: 'ค้นหาด้วย รถ ใหม่', value: 'รถใหม่' },
        { text: 'ค้นหาด้วย ใบเตือน', value: 'ใบเตือน' },
        { text: 'ค้นหาด้วย รถ BP', value: 'รถBP' },
        { text: 'สร้างใหม่', value: 'สร้างใหม่' }
      ],
      optionsSearchDate: [
        { text: 'เดือน/ปี แจ้งประกัน', value: 'แจ้งประกัน' },
        { text: 'เดือน/ปี รับกรมธรรม์', value: 'รับกรมธรรม์' },
        { text: 'เดือน/ปี เริ่มคุ้มครอง', value: 'เริ่มคุ้มครอง' },
        { text: 'เดือน/ปี ใบเตือน', value: 'ใบเตือน' },
        { text: 'เดือน/ปี ส่งจดหมาย', value: 'ส่งจดหมาย' }
      ],
      optionsCustomerReferral: [
        { text: 'ลูกค้าแนะนำ', value: 'ลูกค้าแนะนำ' },
        { text: 'ส่วนงาน CRD', value: 'ส่วนงาน CRD' },
        { text: 'ส่วนงาน CASHIER', value: 'ส่วนงาน CASHIER' },
        { text: 'ส่วนงาน BP', value: 'ส่วนงาน BP' },
        { text: 'ส่วนงาน V.Certified', value: 'ส่วนงาน V.Certified' },
        { text: 'ลูกค้าเก่า', value: 'ลูกค้าเก่า' }
      ],
      optionsParterType: [
        { text: 'ร้านอะไหล่รถยนต์', value: '0010' },
        { text: 'ลีสซิ่งและให้เช่า', value: '0011' },
        { text: 'ลูกค้าขาจร', value: '9999' },
        { text: 'ลูกค้านิติบุคคลทั่วไป', value: '0002' },
        { text: 'พนักงาน (ฮอนด้า)', value: '0012' },
        { text: 'ลูกค้าบุคคลทั่วไป', value: '0001' }
      ],
      optionsCusType: [
        { text: 'บุคคล', value: '1' },
        { text: 'องค์กร', value: '2' }
      ],
      optionsGrade: [
        { text: 'A', value: 'A' },
        { text: 'B', value: 'B' },
        { text: 'C', value: 'C' },
        { text: 'D', value: 'D' },
        { text: 'ไม่ระบุ', value: 'not specified' }
      ],
      optionsIncomingType: [
        { text: 'BP', value: 'BP' },
        { text: 'GR', value: 'GR' },
        { text: 'OTH', value: 'OTH' },
        { text: 'PM', value: 'PM' },
        { text: 'ไม่ระบุ', value: 'not specified' }
      ],
      optionsSex: [
        { text: 'ชาย', value: 'Male' },
        { text: 'หญิง', value: 'Female' }
      ],
      optionsMonth: [
        { text: 'มกราคม', value: 'มกราคม' },
        { text: 'กุมภาพันธ์', value: 'กุมภาพันธ์' },
        { text: 'มีนาคม', value: 'มีนาคม' },
        { text: 'เมษายน', value: 'เมษายน' },
        { text: 'พฤษภาคม', value: 'พฤษภาคม' },
        { text: 'มิถุนายน', value: 'มิถุนายน' },
        { text: 'กรกฎาคม', value: 'กรกฎาคม' },
        { text: 'สิงหาคม', value: 'สิงหาคม' },
        { text: 'กันยายน', value: 'กันยายน' },
        { text: 'ตุลาคม', value: 'ตุลาคม' },
        { text: 'พฤศจิกายน', value: 'พฤศจิกายน' },
        { text: 'ธันวาคม', value: 'ธันวาคม' }
      ],
      optionsFinance: [
        { text: 'ธนาคาร ทิสโก้ จำกัด (มหาชน)', value: 'ธนาคาร ทิสโก้ จำกัด (มหาชน)' },
        { text: 'ธนาคาร ธนชาต จำกัด (มหาชน)', value: 'ธนาคาร ธนชาต จำกัด (มหาชน)' },
        { text: 'ธนาคาร กรุงศรีอยุธยา จำกัด (มหาชน)', value: 'ธนาคาร กรุงศรีอยุธยา จำกัด (มหาชน)' },
        { text: 'ธนาคาร เกียรตินาคิน จำกัด (มหาชน)', value: 'ธนาคาร เกียรตินาคิน จำกัด (มหาชน)' },
        { text: 'ธนาคาร ไทยพาณิชย์ จำกัด', value: 'ธนาคาร ไทยพาณิชย์ จำกัด' },
        { text: 'บริษัท กรุงไทยธุรกิจลีสซิ่ง จำกัด', value: 'บริษัท กรุงไทยธุรกิจลีสซิ่ง จำกัด' },
        { text: 'บริษัท ลีสซิ่ง กสิกรไทย จำกัด', value: 'บริษัท ลีสซิ่ง กสิกรไทย จำกัด' },
        { text: 'บริษัท ฮอนด้า ลีสซิ่ง (ประเทศไทย) จำกัด', value: 'บริษัท ฮอนด้า ลีสซิ่ง (ประเทศไทย) จำกัด' },
        { text: 'ธนาคารทหารไทยธนชาต จำกัด (มหาชน)', value: 'ธนาคารทหารไทยธนชาต จำกัด (มหาชน)' }
      ],
      optionRemarkInsSelect: [
        { text: '1.แจ้งต่อประกันภัย', value: '1.แจ้งต่อประกันภัย' },
        { text: '2.แจ้งต่อผ่านโบรคเกอร์', value: '2.แจ้งต่อผ่านโบรคเกอร์' },
        { text: '3.แจ้งต่อประกันภัยกับญาติ+เพื่อน', value: '3.แจ้งต่อประกันภัยกับญาติ+เพื่อน' },
        { text: '4.แจ้งต่อประกันภัยผ่าน F/N', value: '4.แจ้งต่อประกันภัยผ่าน F/N' },
        { text: '5.เป็นตัวแทนเอง', value: '5.เป็นตัวแทนเอง' },
        { text: '6.แจ้งต่อผ่านบริษัท(ประกันกลุ่ม)', value: '6.แจ้งต่อผ่านบริษัท(ประกันกลุ่ม)' },
        { text: '7.ใช้รถต่างจังหวัด', value: '7.ใช้รถต่างจังหวัด' },
        { text: '8.เบี้ยแพงไม่ทำ', value: '8.เบี้ยแพงไม่ทำ' },
        { text: '9.ติดต่อไม่ได้', value: '9.ติดต่อไม่ได้' },
        { text: '10.ขายรถแล้ว', value: '10.ขายรถแล้ว' },
        { text: '11.ติดตามแล้วรอการตัดสินใจ', value: '11.ติดตามแล้วรอการตัดสินใจ' },
        { text: '12.อื่นๆ', value: '12.อื่นๆ' },
        { text: '13.ไม่มีเงินภาระเยอะ', value: '13.ไม่มีเงินภาระเยอะ' },
        { text: '14.แจ้งผ่านศูนย์ฮอนด้าอื่น', value: '14.แจ้งผ่านศูนย์ฮอนด้าอื่น' },
        { text: '15.มีเคลมไม่ต่อ', value: '15.มีเคลมไม่ต่อ' },
        { text: '16.ไม่พอใจการบริการของประกันฯ', value: '16.ไม่พอใจการบริการของประกันฯ' },
        { text: '17.รถไม่ค่อยได้ใช้งานไม่ต่อ', value: '17.รถไม่ค่อยได้ใช้งานไม่ต่อ' },
        { text: '18.ขอโอนโค๊ตไปต่อที่อื่น', value: '18.ขอโอนโค๊ตไปต่อที่อื่น' }
      ],
      optionInsuranceName: [
        { text: 'บริษัท คุ้มภัยโตเกียวมารีนประกันภัย (ประเทศไทย)', value: 'บริษัท คุ้มภัยโตเกียวมารีนประกันภัย (ประเทศไทย)' },
        { text: 'บริษัท แอกซ่าประกันภัย จำกัด (มหาชน)', value: 'บริษัท แอกซ่าประกันภัย จำกัด (มหาชน)' },
        { text: 'บริษัท กรุงเทพประกันภัย จำกัด (มหาชน)', value: 'บริษัท กรุงเทพประกันภัย จำกัด (มหาชน)' },
        { text: 'บริษัท ทิพยประกันภัย จำกัด (มหาชน)', value: 'บริษัท ทิพยประกันภัย จำกัด (มหาชน)' },
        { text: 'บริษัท มิตซุย สุมิโตโม อินชัวรันซ์ จำกัด สาขาประเทศไทย', value: 'บริษัท มิตซุย สุมิโตโม อินชัวรันซ์ จำกัด สาขาประเทศไทย' },
        { text: 'บริษัท ศรีอยุธยา เจนเนอรัล ประกันภัย จำกัด (มหาชน)', value: 'บริษัท ศรีอยุธยา เจนเนอรัล ประกันภัย จำกัด (มหาชน)' },
        { text: 'บริษัท อาคเนย์ประกันภัย จำกัด (มหาชน)', value: 'บริษัท อาคเนย์ประกันภัย จำกัด (มหาชน)' },
        { text: 'บริษัท ธนชาตประกันภัย จำกัด (มหาชน)', value: 'บริษัท ธนชาตประกันภัย จำกัด (มหาชน)' },
        { text: 'บริษัท ประกันภัยไทยวิวัฒน์ จำกัด (มหาชน)', value: 'บริษัท ประกันภัยไทยวิวัฒน์ จำกัด (มหาชน)' },
        { text: 'บริษัท ซมโปะ ประกันภัย (ประเทศไทย) จำกัด (มหาชน)', value: 'บริษัท ซมโปะ ประกันภัย (ประเทศไทย) จำกัด (มหาชน)' },
        { text: 'บริษัท วิริยะประกันภัย จำกัด (มหาชน)', value: 'บริษัท วิริยะประกันภัย จำกัด (มหาชน)' },
        { text: 'บริษัท คุ้มภัยประกันภัย จำกัด (มหาชน)', value: 'บริษัท คุ้มภัยประกันภัย จำกัด (มหาชน)' },
        { text: 'บริษัท โตเกียวมารีนประกันภัย (ประเทศไทย) จำกัด (มหาชน)', value: 'บริษัท โตเกียวมารีนประกันภัย (ประเทศไทย) จำกัด (มหาชน)' },
        { text: '(ตรง) บริษัท วิริยะประกันภัย จำกัด (มหาชน)', value: '(ตรง) บริษัท วิริยะประกันภัย จำกัด (มหาชน)' },
        { text: '(ตรง) บริษัท คุ้มภัยประกันภัย จำกัด (มหาชน)', value: '(ตรง) บริษัท คุ้มภัยประกันภัย จำกัด (มหาชน)' },
        { text: '(ตรง) บริษัท ทิพยประกันภัย จำกัด (มหาชน)', value: '(ตรง) บริษัท ทิพยประกันภัย จำกัด (มหาชน)' },
        { text: '(ตรง) บริษัท ประกันภัยไทยวิวัฒน์ จำกัด (มหาชน)', value: '(ตรง) บริษัท ประกันภัยไทยวิวัฒน์ จำกัด (มหาชน)' },
        { text: '(ตรง) บริษัท ศรีอยุธยา เจนเนอรัล ประกันภัย จำกัด (มหาชน)', value: '(ตรง) บริษัท ศรีอยุธยา เจนเนอรัล ประกันภัย จำกัด (มหาชน)' },
        { text: '(ตรง) บริษัท อาคเนย์ประกันภัย จำกัด (มหาชน)', value: '(ตรง) บริษัท อาคเนย์ประกันภัย จำกัด (มหาชน)' },
        { text: '(ตรง) บริษัท แอกซ่าประกันภัย จำกัด (มหาชน)', value: '(ตรง) บริษัท แอกซ่าประกันภัย จำกัด (มหาชน)' },
        { text: '(ตรง) บริษัท ชับบ์สามัคคีประกันภัย จำกัด (มหาชน)', value: '(ตรง) บริษัท ชับบ์สามัคคีประกันภัย จำกัด (มหาชน)' }
      ],
      optionCarotherBrand: [
        { text: 'HONDA', value: 'HONDA' },
        { text: 'TOYOTA', value: 'TOYOTA' },
        { text: 'NISSAN', value: 'NISSAN' },
        { text: 'MAZDA', value: 'MAZDA' },
        { text: 'CHEVROLET', value: 'CHEVROLET' },
        { text: 'FORD', value: 'FORD' },
        { text: 'ISUZU', value: 'ISUZU' },
        { text: 'HYUNDAI', value: 'HYUNDAI' },
        { text: 'BMW', value: 'BMW' },
        { text: 'Mercedes-Benz', value: 'Mercedes-Benz' },
        { text: 'PROTON', value: 'PROTON' },
        { text: 'Suzuki', value: 'Suzuki' },
        { text: 'MG', value: 'MG' },
        { text: 'MINI COOPER', value: 'MINI COOPER' }
      ],
      optionCarotherModelBMW: [
        { text: '318i', value: '318i' },
        { text: '23i', value: '23i' },
        { text: '523i', value: '523i' },
        { text: '525i', value: '525i' },
        { text: '723i', value: '723i' },
        { text: '730Li', value: '730Li' }
      ],
      optionCarotherModelCHEVROLET: [
        { text: 'AVEO', value: 'AVEO' },
        { text: 'OPTRA', value: 'OPTRA' },
        { text: 'ZAFIRA', value: 'ZAFIRA' },
        { text: 'CAPTIVA', value: 'CAPTIVA' },
        { text: 'CRUZE', value: 'CRUZE' },
        { text: 'CAMARO', value: 'CAMARO' },
        { text: 'COLORADO', value: 'COLORADO' },
        { text: 'TRAILBLAZER', value: 'TRAILBLAZER' }
      ],
      optionCarotherModelFORD: [
        { text: 'RANGER', value: 'RANGER' },
        { text: 'FIESTA', value: 'FIESTA' },
        { text: 'FOCUS', value: 'FOCUS' },
        { text: 'FESTIVA', value: 'FESTIVA' },
        { text: 'ASPIRE', value: 'ASPIRE' },
        { text: 'ESCAPE', value: 'ESCAPE' },
        { text: 'EVEREST', value: 'EVEREST' },
        { text: 'EXPLORER', value: 'EXPLORER' }
      ],
      optionCarotherModelHYUNDAI: [
        { text: 'H1', value: 'H1' }
      ],
      optionCarotherModelISUZU: [
        { text: 'MU-7', value: 'MU-7' },
        { text: 'SPACECAB', value: 'SPACECAB' },
        { text: '4 ประตู 2.5', value: '4 ประตู 2.5' },
        { text: 'D-MAX', value: 'D-MAX' },
        { text: 'DARGON EYE', value: 'DARGON EYE' },
        { text: 'DRAGON POWER', value: 'DRAGON POWER' }
      ],
      optionCarotherModelMAZDA: [
        { text: '2', value: '2' },
        { text: '3', value: '3' }
      ],
      optionCarotherModelMercedesBenz: [
        { text: 'C220', value: 'C220' },
        { text: 'E200', value: 'E200' },
        { text: 'C180', value: 'C180' },
        { text: 'E220', value: 'E220' }
      ],
      optionCarotherModelMG: [
        { text: 'MG5', value: 'MG5' }
      ],
      optionCarotherModelMINICOOPER: [
        { text: 'S RHD', value: 'S RHD' }
      ],
      optionCarotherModelMISUBISHI: [
        { text: 'MIRAGE', value: 'MIRAGE' },
        { text: 'LANCER', value: 'LANCER' },
        { text: 'SPACEWAGON', value: 'SPACEWAGON' },
        { text: 'PAJERO', value: 'PAJERO' },
        { text: 'TRITON', value: 'TRITON' }
      ],
      optionCarotherModelNISSAN: [
        { text: 'MARCH', value: 'MARCH' },
        { text: 'ALMERA', value: 'ALMERA' },
        { text: 'PULSAR', value: 'PULSAR' },
        { text: 'SYLPHY', value: 'SYLPHY' },
        { text: 'NEO', value: 'NEO' },
        { text: 'JUKE', value: 'JUKE' },
        { text: 'SUNNY', value: 'SUNNY' },
        { text: 'TIIDA', value: 'TIIDA' },
        { text: 'TEANA', value: 'TEANA' },
        { text: 'CEFIRO', value: 'CEFIRO' },
        { text: 'NV', value: 'NV' },
        { text: 'GL', value: 'GL' },
        { text: 'NAVARA', value: 'NAVARA' },
        { text: 'BIG -M', value: 'BIG -M' },
        { text: 'NOTE', value: 'NOTE' }
      ],
      optionCarotherModelPROTON: [
        { text: 'SAVVY', value: 'SAVVY' }
      ],
      optionCarotherModelSuzuki: [
        { text: 'Swift', value: 'Swift' }
      ],
      optionCarotherModelTOYOTA: [
        { text: 'YARIS', value: 'YARIS' },
        { text: 'VIOS', value: 'VIOS' },
        { text: 'AVANZA', value: 'AVANZA' },
        { text: 'ALTIS', value: 'ALTIS' },
        { text: 'CAMRY', value: 'CAMRY' },
        { text: 'ESTIMA', value: 'ESTIMA' },
        { text: 'COROLLA', value: 'COROLLA' },
        { text: 'SOLUNA', value: 'SOLUNA' },
        { text: 'CORONA', value: 'CORONA' },
        { text: 'INNOVA', value: 'INNOVA' },
        { text: 'WISH', value: 'WISH' },
        { text: 'FORTUNER', value: 'FORTUNER' },
        { text: 'HILUX', value: 'HILUX' },
        { text: 'VIGO', value: 'VIGO' },
        { text: 'MIGHTY X', value: 'MIGHTY X' },
        { text: 'D4D', value: 'D4D' },
        { text: 'TIGER', value: 'TIGER' },
        { text: 'ALPHARD', value: 'ALPHARD' }
      ],
      optionCarotherModelHONDA: [
        { text: 'BRIO AMAZE', value: 'BRIO AMAZE' },
        { text: 'BRIO', value: 'BRIO' },
        { text: 'CITY', value: 'CITY' },
        { text: 'CITY CNG', value: 'CITY CNG' },
        { text: 'CITY e:HEV', value: 'CITY e:HEV' },
        { text: 'CITY HATCHBACK', value: 'CITY HATCHBACK' },
        { text: 'JAZZ', value: 'JAZZ' },
        { text: 'JAZZ HYBRID', value: 'JAZZ HYBRID' },
        { text: 'CIVIC', value: 'CIVIC' },
        { text: 'CIVIC HYBRID', value: 'CIVIC HYBRID' },
        { text: 'CIVIC HATCHBACK', value: 'CIVIC HATCHBACK' },
        { text: 'FREED', value: 'FREED' },
        { text: 'HR-V', value: 'HR-V' },
        { text: 'BR-V', value: 'BR-V' },
        { text: 'CR-V', value: 'CR-V' },
        { text: 'CR-Z', value: 'CR-Z' },
        { text: 'ACCORD', value: 'ACCORD' },
        { text: 'ACCORD HYBRID', value: 'ACCORD HYBRID' },
        { text: 'RX', value: 'RX' },
        { text: 'STREAM', value: 'STREAM' },
        { text: 'STEPWGNSPADA', value: 'STEPWGNSPADA' },
        { text: 'ODYSSEY', value: 'ODYSSEY' },
        { text: 'MOBILIO', value: 'MOBILIO' }
      ],
      optionCarotherColor: [
        { text: 'สีแดง', value: 'สีแดง' },
        { text: 'สีน้ำเงิน', value: 'สีน้ำเงิน' },
        { text: 'สีเหลือง', value: 'สีเหลือง' },
        { text: 'สีขาว', value: 'สีขาว' },
        { text: 'สีดำ', value: 'สีดำ' },
        { text: 'สีม่วง', value: 'สีม่วง' },
        { text: 'สีเขียว', value: 'สีเขียว' },
        { text: 'สีส้ม', value: 'สีส้ม' },
        { text: 'สีน้ำตาล', value: 'สีน้ำตาล' },
        { text: 'สีชมพู', value: 'สีชมพู' },
        { text: 'สีเทา', value: 'สีเทา' },
        { text: 'สีฟ้า', value: 'สีฟ้า' }
      ],
      optionsbranchNo: [
        { text: 'สาขาแจ้งวัฒนะ', value: '0112' },
        { text: 'สาขาหนองแขม', value: '0113' },
        { text: 'สาขาบางกอกน้อย', value: '0115' },
        { text: 'สาขาปากเกร็ด - เมืองทอง', value: '0116' },
        { text: 'สำนักงานใหญ่', value: '333' }
      ],
      optionTypeSend: [
        { text: 'ยังไม่ส่งกรมธรรม์', value: 'ยังไม่ส่งกรมธรรม์' },
        { text: 'กรมธรรม์ยังไม่มา', value: 'กรมธรรม์ยังไม่มา' },
        { text: 'ส่งคืนประกัน', value: 'ส่งคืนประกัน' },
        { text: 'ส่งตาม EMS', value: 'ส่งตาม EMS' },
        { text: 'สาขาแจ้งวัฒนะ', value: '0112' },
        { text: 'สาขาหนองแขม', value: '0113' },
        { text: 'สาขาบางกอกน้อย', value: '0115' },
        { text: 'สาขาปากเกร็ด - เมืองทอง', value: '0116' },
        { text: 'สำนักงานใหญ่', value: '333' }
      ],
      optionsNoHObranchNo: [
        { text: 'สาขาแจ้งวัฒนะ', value: '0112' },
        { text: 'สาขาหนองแขม', value: '0113' },
        { text: 'สาขาบางกอกน้อย', value: '0115' },
        { text: 'สาขาปากเกร็ด - เมืองทอง', value: '0116' }
      ],
      optionsbranchNoOld: [
        { text: 'สาขาแจ้งวัฒนะ', value: '10112' },
        { text: 'สาขาหนองแขม', value: '10115' },
        { text: 'สาขาบางกอกน้อย', value: '10126' },
        { text: 'สาขาปากเกร็ด - เมืองทอง', value: '10143' }
      ],
      optionsPaymentType: [
        { text: 'ไฟแนนซ์', value: 'ไฟแนนซ์' },
        { text: 'เงินสด', value: 'เงินสด' }
      ],
      optionsTypeRepair: [
        { text: 'ซ่อมศูนย์', value: 'ซ่อมศูนย์' },
        { text: 'ซ่อมอู่', value: 'ซ่อมอู่' }
      ],
      optionsTypeProtec: [
        { text: 'ประเภท 1', value: '1' },
        { text: 'ประเภท 2', value: '2' },
        { text: 'ประเภท 2+', value: '2+' },
        { text: 'ประเภท 3+', value: '3+' },
        { text: 'ประเภท 3', value: '3' }
      ],
      optionsTypeIns: [
        { text: 'ไม่ระบุชื่อ', value: 'ไม่ระบุชื่อ' },
        { text: 'ระบุชื่อ 18-24', value: 'ระบุชื่อ 18-24' },
        { text: 'ระบุชื่อ 25-35', value: 'ระบุชื่อ 25-35' },
        { text: 'ระบุชื่อ 36-50', value: 'ระบุชื่อ 36-50' },
        { text: 'ระบุชื่อ >50', value: 'ระบุชื่อ >50' }
      ],
      optionsTypeNew: [
        { text: '1 ป้ายแดง ซื้อ', value: '1 ป้ายแดง ซื้อ' },
        { text: '2 ป้ายแดง วี.กรุ๊ป', value: '2 ป้ายแดง วี.กรุ๊ป' },
        { text: '3 ป้ายแดง Honda', value: '3 ป้ายแดง Honda' },
        { text: '4 ป้ายแดง พรบ.', value: '4 ป้ายแดง พรบ.' },
        { text: '5 ป้ายแดง ส่วนต่าง', value: '5 ป้ายแดง ส่วนต่าง' },
        { text: '6 ต่ออายุ', value: '6 ต่ออายุ' },
        { text: '7 ต่ออายุ พรบ.', value: '7 ต่ออายุ พรบ.' },
        { text: '8 ต่ออายุ ส่วนต่าง', value: '8 ต่ออายุ ส่วนต่าง' }
      ],
      optionProvince: [
        { text: 'กรุงเทพมหานคร', value: 'กรุงเทพมหานคร' },
        { text: 'สมุทรปราการ', value: 'สมุทรปราการ' },
        { text: 'นนทบุรี', value: 'นนทบุรี' },
        { text: 'ปทุมธานี', value: 'ปทุมธานี' },
        { text: 'พระนครศรีอยุธยา', value: 'พระนครศรีอยุธยา' },
        { text: 'อ่างทอง', value: 'อ่างทอง' },
        { text: 'ลพบุรี', value: 'ลพบุรี' },
        { text: 'สิงห์บุรี', value: 'สิงห์บุรี' },
        { text: 'ชัยนาท', value: 'ชัยนาท' },
        { text: 'สระบุรี', value: 'สระบุรี' },
        { text: 'ชลบุรี', value: 'ชลบุรี' },
        { text: 'ระยอง', value: 'ระยอง' },
        { text: 'จันทบุรี', value: 'จันทบุรี' },
        { text: 'ตราด', value: 'ตราด' },
        { text: 'ฉะเชิงเทรา', value: 'ฉะเชิงเทรา' },
        { text: 'ปราจีนบุรี', value: 'ปราจีนบุรี' },
        { text: 'นครนายก', value: 'นครนายก' },
        { text: 'สระแก้ว', value: 'สระแก้ว' },
        { text: 'นครราชสีมา', value: 'นครราชสีมา' },
        { text: 'บุรีรัมย์', value: 'บุรีรัมย์' },
        { text: 'สุรินทร์', value: 'สุรินทร์' },
        { text: 'ศรีสะเกษ', value: 'ศรีสะเกษ' },
        { text: 'อุบลราชธานี', value: 'อุบลราชธานี' },
        { text: 'ยโสธร', value: 'ยโสธร' },
        { text: 'ชัยภูมิ', value: 'ชัยภูมิ' },
        { text: 'อำนาจเจริญ', value: 'อำนาจเจริญ' },
        { text: 'บึงกาฬ', value: 'บึงกาฬ' },
        { text: 'หนองบัวลำภู', value: 'หนองบัวลำภู' },
        { text: 'ขอนแก่น', value: 'ขอนแก่น' },
        { text: 'อุดรธานี', value: 'อุดรธานี' },
        { text: 'เลย', value: 'เลย' },
        { text: 'หนองคาย', value: 'หนองคาย' },
        { text: 'มหาสารคาม', value: 'มหาสารคาม' },
        { text: 'ร้อยเอ็ด', value: 'ร้อยเอ็ด' },
        { text: 'กาฬสินธุ์', value: 'กาฬสินธุ์' },
        { text: 'สกลนคร', value: 'สกลนคร' },
        { text: 'นครพนม', value: 'นครพนม' },
        { text: 'มุกดาหาร', value: 'มุกดาหาร' },
        { text: 'เชียงใหม่', value: 'เชียงใหม่' },
        { text: 'ลำพูน', value: 'ลำพูน' },
        { text: 'ลำปาง', value: 'ลำปาง' },
        { text: 'อุตรดิตถ์', value: 'อุตรดิตถ์' },
        { text: 'แพร่', value: 'แพร่' },
        { text: 'น่าน', value: 'น่าน' },
        { text: 'พะเยา', value: 'พะเยา' },
        { text: 'เชียงราย', value: 'เชียงราย' },
        { text: 'แม่ฮ่องสอน', value: 'แม่ฮ่องสอน' },
        { text: 'นครสวรรค์', value: 'นครสวรรค์' },
        { text: 'อุทัยธานี', value: 'อุทัยธานี' },
        { text: 'กำแพงเพชร', value: 'กำแพงเพชร' },
        { text: 'ตาก', value: 'ตาก' },
        { text: 'สุโขทัย', value: 'สุโขทัย' },
        { text: 'พิษณุโลก', value: 'พิษณุโลก' },
        { text: 'พิจิตร', value: 'พิจิตร' },
        { text: 'เพชรบูรณ์', value: 'เพชรบูรณ์' },
        { text: 'ราชบุรี', value: 'ราชบุรี' },
        { text: 'กาญจนบุรี', value: 'กาญจนบุรี' },
        { text: 'สุพรรณบุรี', value: 'สุพรรณบุรี' },
        { text: 'นครปฐม', value: 'นครปฐม' },
        { text: 'สมุทรสาคร', value: 'สมุทรสาคร' },
        { text: 'สมุทรสงคราม', value: 'สมุทรสงคราม' },
        { text: 'เพชรบุรี', value: 'เพชรบุรี' },
        { text: 'ประจวบคีรีขันธ์', value: 'ประจวบคีรีขันธ์' },
        { text: 'นครศรีธรรมราช', value: 'นครศรีธรรมราช' },
        { text: 'กระบี่', value: 'กระบี่' },
        { text: 'พังงา', value: 'พังงา' },
        { text: 'ภูเก็ต', value: 'ภูเก็ต' },
        { text: 'สุราษฎร์ธานี', value: 'สุราษฎร์ธานี' },
        { text: 'ระนอง', value: 'ระนอง' },
        { text: 'ชุมพร', value: 'ชุมพร' },
        { text: 'สงขลา', value: 'สงขลา' },
        { text: 'สตูล', value: 'สตูล' },
        { text: 'ตรัง', value: 'ตรัง' },
        { text: 'พัทลุง', value: 'พัทลุง' },
        { text: 'ปัตตานี', value: 'ปัตตานี' },
        { text: 'ยะลา', value: 'ยะลา' },
        { text: 'นราธิวาส', value: 'นราธิวาส' }
      ]
    }
  },
  async mounted () {
    if (process.env.NODE_ENV === 'development') {
      this.DNS_IP = this.IPPotocalENV_Developer
    } else {
      this.DNS_IP = this.IPPotocalENV_Production
    }
  },
  methods: {
    formatNumber (value) {
      if (value) {
        return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',')
      }
    },
    format_date (value) {
      if (value) {
        return moment(String(value)).format('DD/MM/YYYY HH:mm:ss')
      }
    },
    format_dateNotime (value) {
      if (value) {
        return moment(String(value)).format('DD/MM/YYYY')
      }
    },
    // YYYY-MM-DD
    momenDate_1 (value) {
      if (value) {
        return moment(String(value)).format('YYYY-MM-DD')
      }
    },
    momenDate_2 (value) {
      if (value) {
        return moment(String(value)).format('YYYY-MM-DD HH:mm:ss')
      }
    }
  }
})
Vue.component('my-currency-input', {
  props: ['value'],
  template: `
          <input type="text" v-model="displayValue" @blur="isInputActive = false" @focus="isInputActive = true"/>
     `,
  data: function () {
    return {
      isInputActive: false
    }
  },
  computed: {
    displayValue: {
      get: function () {
        if (this.isInputActive) {
          // Cursor is inside the input field. unformat display value for user
          return this.value.toString()
        } else {
          // User is not modifying now. Format display value for user interface
          return this.value
            .toFixed(2)
            .replace(/(\d)(?=(\d{3})+(?:\.\d+)?$)/g, '$1,')
        }
      },
      set: function (modifiedValue) {
        // Recalculate value after ignoring "$" and "," in user input
        // eslint-disable-next-line no-useless-escape
        let newValue = parseFloat(modifiedValue.replace(/[^\d\.]/g, ''))
        // Ensure that it is not NaN
        if (isNaN(newValue)) {
          newValue = 0
        }
        // Note: we cannot set this.value as it is a "prop". It needs to be passed to parent component
        // $emit the event so that parent component gets it
        this.$emit('input', newValue)
      }
    }
  }
})

/* eslint-disable no-new */
new Vue({
  el: '#app',
  vuetify,
  router,
  components: { App },
  template: '<App/>'
})
